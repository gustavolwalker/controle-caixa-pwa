import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Salesman } from '../salesman';
import { SalesmansService } from '../salesmans.service';

@Component({
  selector: 'app-salesmans-list',
  templateUrl: './salesmans-list.component.html',
  styleUrls: ['./salesmans-list.component.css']
})
export class SalesmansListComponent implements OnInit {

  @ViewChild('pesquisarInput', { static: false }) pesquisarInput: ElementRef<HTMLInputElement>;
  salesmans: Salesman[] = [];
  filter: string = '';
  hasMore: boolean = true;
  currentPage: number = 1;

  constructor(
    private activatedRoute: ActivatedRoute,
    private service: SalesmansService
  ) { }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe(params => {
      this.salesmans = this.activatedRoute.snapshot.data['salesmans'];
    });
  }

  pesquisar() {

    console.log("vou pesquisar: " + this.pesquisarInput.nativeElement.value);
  }

  load() {

    this.service
      .listAllPaginated(++this.currentPage)
      .subscribe(salesmans => {
        this.filter = '';
        this.salesmans = this.salesmans.concat(salesmans);
        if (!salesmans.length) this.hasMore = false;
      });
  }

  remove(id: number) {
    this.service
      .remove(id)
      .subscribe(() => {
        this.salesmans = this.salesmans.filter((value) => {
          return value.id != id;
        });
      });
  }
}
