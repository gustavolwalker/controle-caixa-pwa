import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesmansListComponent } from './salesmans-list.component';

describe('SalesmansListComponent', () => {
  let component: SalesmansListComponent;
  let fixture: ComponentFixture<SalesmansListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesmansListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesmansListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
