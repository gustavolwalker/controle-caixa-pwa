import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Salesman } from '../salesman';
import { SalesmansService } from '../salesmans.service';

@Injectable({ providedIn: 'root' })
export class SalesmansListResolver implements Resolve<Observable<Salesman[]>>{

    constructor(private service: SalesmansService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Salesman[]> {        
        return this.service.listAllPaginated(1);
    }
}