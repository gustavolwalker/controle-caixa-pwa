import { Pipe, PipeTransform } from '@angular/core';
import { Salesman } from '../salesman';

@Pipe({
  name: 'filterByIdNames'
})
export class FilterByIdNamesPipe implements PipeTransform {

  transform(salesmans: Salesman[], filterQuery: string): Salesman[] {
    filterQuery = filterQuery
      .trim()
      .toLowerCase();

    if (filterQuery) {
      return salesmans.filter(salesman =>
        salesman.id.toString()
          .toLowerCase()
          .concat(
            ' ',
            salesman.nickName.toLowerCase(),
            ' ',
            salesman.fullName.toLowerCase()
          )
          .includes(filterQuery)
      );
    } else {
      return salesmans;
    }
  }

}
