import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, ActivationStart } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptor } from '../core/auth/request.interceptor';
import { SalesmansListComponent } from './salesmans-list/salesmans-list.component';
import { SalesmansFormComponent } from './salesmans-form/salesmans-form.component';
import { VMessageModule } from '../shared/components/vmessage/vmessage.module';
import { ActivedModule } from '../shared/pipes/actived/actived.module';
import { FilterListModule } from '../shared/components/filter-list/filter-list.module';
import { FilterByIdNamesPipe } from './salesmans-list/filter-by-id-names.pipe';
import { BackButtonModule } from '../shared/directives/back-button/back-button.module';

@NgModule({
  declarations: [
    SalesmansListComponent, 
    SalesmansFormComponent,
    FilterByIdNamesPipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    VMessageModule,    
    ActivedModule,
    BackButtonModule,    
    FilterListModule
  ],
  providers: [
      {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestInterceptor,
          multi: true
      }
  ]
})
export class SalesmansModule { }
