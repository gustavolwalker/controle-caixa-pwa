import { Inject } from '@angular/core';
import { Salesman } from './salesman';

@Inject({
  providedIn: 'root'
})
export class SalesmansDaoService {

  private store = 'salesmans';
  constructor(private connection) { }

  insert(salesman: Salesman) {

    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .add(salesman);

      request.onsuccess = e => {

        resolve();
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject('Não foi possível inserir o vendedor');

      };
    });
  }

  update(salesman: Salesman) {

    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .put(salesman);

      request.onsuccess = e => {

        resolve();
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject('Não foi possível atualizar o vendedor');

      };
    });
  }

  delete(id: number) {
    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .delete(id);

      request.onsuccess = e => {

        resolve();
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject('Não foi possível excluir o vendedor');

      };
    });
  }

  findById(id: number) {

    return new Promise<Salesman>((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readonly')
        .objectStore(this.store)
        .get(id);

      request.onsuccess = e => {

        console.log(`Dao get ${id} result ${e.target.result}`);
        resolve(e.target.result);
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject(`Não foi possível encontrar o vendedor: ${id}`);
      };
    });
  }

  findAll(page: number) {
    return new Promise<Salesman[]>((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readonly')
        .objectStore(this.store)
        .getAll();

      request.onsuccess = e => {

        resolve(e.target.result);
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject(`Não foi possível buscar os vendedores`);
      };
    });
  }


}
