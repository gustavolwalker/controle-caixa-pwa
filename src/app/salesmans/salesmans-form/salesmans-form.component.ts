import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PlatformDetectorService } from 'src/app/core/platform-detector/platform-detector.service';
import { Salesman } from '../salesman';
import { SalesmansService } from '../salesmans.service';

@Component({
    selector: 'app-salesmans-form',
    templateUrl: './salesmans-form.component.html',
    styleUrls: ['./salesmans-form.component.css']
})
export class SalesmansFormComponent implements OnInit {

    salesman: Salesman;
    salesmanForm: FormGroup;
    @ViewChild('nickNameInput', { static: true }) nickNameInput: ElementRef<HTMLInputElement>;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private platformDetectorService: PlatformDetectorService,
        private service: SalesmansService
    ) { }

    ngOnInit(): void {

        this.salesmanForm = this.formBuilder.group({
            id: [null],
            fullName: ['',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(60)
                ]
            ],
            nickName: ['',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(8)
                ]
            ],
            actived: [false]
        });

        this.activatedRoute.params
            .subscribe((params: Params) => {
                if (params && params.id) {
                    let id: number = +params.id;
                    console.log(id);
                    this.service.get(id)
                        .subscribe(salesman => {
                            this.salesman = salesman;

                            console.log(this.salesman)
                            if (this.salesman) {
                                this.salesmanForm.patchValue({
                                    id: this.salesman.id,
                                    fullName: this.salesman.fullName,
                                    nickName: this.salesman.nickName,
                                    actived: this.salesman.actived
                                });
                            }
                        })
                }
            });

        this.platformDetectorService.isPlatformBrowser() &&
            this.nickNameInput.nativeElement.focus();
    }

    save() {

        if (this.salesmanForm.valid && !this.salesmanForm.pending) {
            const salesman = this.salesmanForm.getRawValue() as Salesman;
            this.service
                .save(salesman)
                .subscribe(
                    () => this.router.navigate(['vendedor']),
                    err => console.log(err)
                );
        }
    }

}
