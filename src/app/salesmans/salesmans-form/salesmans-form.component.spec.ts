import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesmansFormComponent } from './salesmans-form.component';

describe('SalesmansFormComponent', () => {
  let component: SalesmansFormComponent;
  let fixture: ComponentFixture<SalesmansFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesmansFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesmansFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
