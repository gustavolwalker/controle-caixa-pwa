import { TestBed } from '@angular/core/testing';

import { SalesmansService } from './salesmans.service';

describe('SalesmansService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SalesmansService = TestBed.get(SalesmansService);
    expect(service).toBeTruthy();
  });
});
