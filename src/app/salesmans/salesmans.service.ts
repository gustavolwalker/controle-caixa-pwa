import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IdbService } from '../core/idb/idb.service';
import { Salesman } from './salesman';
import { SalesmansDaoService } from './salesmans-dao.service';

@Injectable({
  providedIn: 'root'
})
export class SalesmansService {

  constructor(
    private idbService: IdbService
  ) { }

  listAllPaginated(page: number) {

    return new Observable<Salesman[]>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new SalesmansDaoService(conn))
        .then(dao => dao.findAll(page))
        .then(salesmans => {
          subscriber.next(salesmans);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar vendedores");
          subscriber.complete();
        });
    });
  }

  listAllSliced(size: number) {

    return new Observable<[Salesman[]]>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new SalesmansDaoService(conn))
        .then(dao => dao.findAll(null))
        .then(salesmans => {
          let parentArray: [Salesman[]] = [[]];
          let childArray: Salesman[] = [];
          salesmans.forEach((item: Salesman, index) => {
            childArray.push(item);
            if (childArray.length === size || index === salesmans.length-1) {
              parentArray.push(childArray);
              childArray = [];
            }
          });
          subscriber.next(parentArray);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar vendedores");
          subscriber.complete();
        });
    });
  }


  get(id: number) {

    return new Observable<Salesman>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new SalesmansDaoService(conn))
        .then(dao => dao.findById(id))
        .then(salesman => {
          subscriber.next(salesman);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar vendedor: " + id);
          subscriber.complete();
        });
    });
  }

  save(salesman: Salesman) {

    if (!salesman.id || salesman.id == 0) {
      delete salesman.id;
      return new Observable(subscriber => {
        this.idbService.getConnection()
          .then(conn => new SalesmansDaoService(conn))
          .then(dao => dao.insert(salesman))
          .then(() => {
            subscriber.next('Vendedor adicionado com sucesso');
            subscriber.complete();
          })
          .catch(erro => {
            console.log(erro);
            subscriber.error("Erro ao inserir vendedor");
            subscriber.complete();
          });
      });
    } else {
      return new Observable(subscriber => {
        this.idbService.getConnection()
          .then(conn => new SalesmansDaoService(conn))
          .then(dao => dao.update(salesman))
          .then(() => {
            subscriber.next('Vendedor editado com sucesso');
            subscriber.complete();
          })
          .catch(erro => {
            console.log(erro);
            subscriber.error("Erro ao editar vendedor");
            subscriber.complete();
          });
      });
    }
  }

  remove(id: number) {
    return new Observable<Boolean>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new SalesmansDaoService(conn))
        .then(dao => dao.delete(id))
        .then(() => {
          subscriber.next(true);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao remover vendedor: " + id);
          subscriber.complete();
        });
    });
  }
}
