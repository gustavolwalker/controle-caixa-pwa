export interface Salesman {
    id: number,
    fullName: string,
    nickName: string,
    actived: boolean
}