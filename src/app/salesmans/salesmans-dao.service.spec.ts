import { TestBed } from '@angular/core/testing';

import { SalesmansDaoService } from './salesmans-dao.service';

describe('SalesmansDaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SalesmansDaoService = TestBed.get(SalesmansDaoService);
    expect(service).toBeTruthy();
  });
});
