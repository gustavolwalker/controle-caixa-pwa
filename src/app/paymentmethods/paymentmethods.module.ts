import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptor } from '../core/auth/request.interceptor';
import { PaymentmethodsListComponent } from './paymentmethods-list/paymentmethods-list.component';
import { PaymentmethodsFormComponent } from './paymentmethods-form/paymentmethods-form.component';
import { ActivedModule } from '../shared/pipes/actived/actived.module';
import { BackButtonModule } from '../shared/directives/back-button/back-button.module';
import { FilterByNamesPipe } from './paymentmethods-list/filter-by-names.pipe';
import { FilterListModule } from '../shared/components/filter-list/filter-list.module';
import { VMessageModule } from '../shared/components/vmessage/vmessage.module';


@NgModule({
  declarations: [
    PaymentmethodsListComponent,
    PaymentmethodsFormComponent,
    FilterByNamesPipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ActivedModule,
    BackButtonModule,    
    FilterListModule,
    VMessageModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    }
  ]
})
export class PaymentmethodsModule { }
