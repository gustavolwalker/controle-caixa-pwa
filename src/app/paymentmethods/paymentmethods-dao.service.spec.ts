import { TestBed } from '@angular/core/testing';

import { PaymentmethodsDaoService } from './paymentmethods-dao.service';

describe('PaymentmethodsDaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentmethodsDaoService = TestBed.get(PaymentmethodsDaoService);
    expect(service).toBeTruthy();
  });
});
