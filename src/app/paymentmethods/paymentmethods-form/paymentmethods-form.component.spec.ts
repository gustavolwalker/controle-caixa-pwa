import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentmethodsFormComponent } from './paymentmethods-form.component';

describe('PaymentmethodsFormComponent', () => {
  let component: PaymentmethodsFormComponent;
  let fixture: ComponentFixture<PaymentmethodsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentmethodsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentmethodsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
