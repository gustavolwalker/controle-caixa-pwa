import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PlatformDetectorService } from 'src/app/core/platform-detector/platform-detector.service';
import { Paymentmethod } from '../paymentmethod';
import { PaymentmethodsService } from '../paymentmethods.service';
import { NickNameNotTakenValidatorService } from './nick-name-not-taken.validator.service';

@Component({
  selector: 'app-paymentmethods-form',
  templateUrl: './paymentmethods-form.component.html',
  styleUrls: ['./paymentmethods-form.component.css'],
  providers: [NickNameNotTakenValidatorService]
})
export class PaymentmethodsFormComponent implements OnInit {

  paymentmethod: Paymentmethod;
  paymentmethodForm: FormGroup;
  @ViewChild('nickNameInput', { static: true }) nickNameInput: ElementRef<HTMLInputElement>;
  @ViewChild('fullNameInput', { static: true }) fullNameInput: ElementRef<HTMLInputElement>;

  constructor(
    private formBuilder: FormBuilder,
    private nickNameNotTakenValidatorService: NickNameNotTakenValidatorService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private platformDetectorService: PlatformDetectorService,
    private service: PaymentmethodsService
  ) { }

  ngOnInit(): void {

    this.paymentmethodForm = this.formBuilder.group({
      nickName: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(16)
        ],
        this.nickNameNotTakenValidatorService.checkNickNameTaken()
      ],
      fullName: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(60)
        ]
      ],
      actived: [false]
    });

    this.platformDetectorService.isPlatformBrowser() &&
      this.nickNameInput.nativeElement.focus();

    this.activatedRoute.params
      .subscribe((params: Params) => {
        if (params && params.id) {
          let id: string = params.id;
          console.log(id);
          this.service.get(id)
            .subscribe(paymentmethod => {
              this.paymentmethod = paymentmethod;

              console.log(this.paymentmethod)
              if (this.paymentmethod) {
                this.paymentmethodForm.patchValue({
                  nickName: this.paymentmethod.nickName,
                  fullName: this.paymentmethod.fullName,
                  actived: this.paymentmethod.actived
                });
                const nicknameControl = this.paymentmethodForm.get('nickName');
                if (this.paymentmethod.nickName) {
                  nicknameControl.disable();
                  this.platformDetectorService.isPlatformBrowser() &&
                    this.fullNameInput.nativeElement.focus();
                }
              }
            })
        }
      });
  }

  save() {

    if (this.paymentmethodForm.valid && !this.paymentmethodForm.pending) {
      const paymentmethod = this.paymentmethodForm.getRawValue() as Paymentmethod;
      this.service
        .save(paymentmethod)
        .subscribe(
          () => this.router.navigate(['formadepagamento']),
          err => console.log(err)
        );
    }
  }
}
