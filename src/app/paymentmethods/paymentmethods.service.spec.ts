import { TestBed } from '@angular/core/testing';

import { PaymentmethodsService } from './paymentmethods.service';

describe('PaymentmethodsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentmethodsService = TestBed.get(PaymentmethodsService);
    expect(service).toBeTruthy();
  });
});
