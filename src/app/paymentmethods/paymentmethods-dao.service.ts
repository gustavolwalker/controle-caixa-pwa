import { Inject } from '@angular/core';
import { Paymentmethod } from './paymentmethod';

@Inject({
  providedIn: 'root'
})
export class PaymentmethodsDaoService {

  private store = 'paymentmethods';
  constructor(private connection) { }

  insert(paymentmethod: Paymentmethod) {

    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .add(paymentmethod);

      request.onsuccess = e => {

        resolve();
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject('Não foi possível inserir a forma de pagamento');

      };
    });
  }

  update(paymentmethod: Paymentmethod) {

    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .put(paymentmethod);

      request.onsuccess = e => {

        resolve();
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject('Não foi possível atualizar a forma de pagamento');

      };
    });
  }

  delete(id: string) {
    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .delete(id);

      request.onsuccess = e => {

        resolve();
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject('Não foi possível excluir a forma de pagamento');

      };
    });
  }

  findById(id: string) {

    return new Promise<Paymentmethod>((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readonly')
        .objectStore(this.store)
        .get(id);

      request.onsuccess = e => {

        console.log(`Dao get ${id} result ${e.target.result}`);
        resolve(e.target.result);
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject(`Não foi possível encontrar a forma de pagamento: ${id}`);
      };
    });
  }

  findAll(page: number) {
    return new Promise<Paymentmethod[]>((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readonly')
        .objectStore(this.store)
        .getAll();

      request.onsuccess = e => {

        resolve(e.target.result);
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject(`Não foi possível buscar as formas de pagamento`);
      };
    });
  }

}
