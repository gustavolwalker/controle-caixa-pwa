import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentmethodsListComponent } from './paymentmethods-list.component';

describe('PaymentmethodsListComponent', () => {
  let component: PaymentmethodsListComponent;
  let fixture: ComponentFixture<PaymentmethodsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentmethodsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentmethodsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
