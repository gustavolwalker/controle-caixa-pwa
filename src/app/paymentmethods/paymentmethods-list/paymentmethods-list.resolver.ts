import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Paymentmethod } from '../paymentmethod';
import { PaymentmethodsService } from '../paymentmethods.service';

@Injectable({ providedIn: 'root' })
export class PaymentmethodsListResolver implements Resolve<Observable<Paymentmethod[]>>{

    constructor(private service: PaymentmethodsService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Paymentmethod[]> {
        return this.service.listAllPaginated(1);
    }
}