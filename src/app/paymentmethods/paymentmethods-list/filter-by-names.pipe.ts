import { Pipe, PipeTransform } from '@angular/core';
import { Paymentmethod } from '../paymentmethod';

@Pipe({
  name: 'filterByNames'
})
export class FilterByNamesPipe implements PipeTransform {

  transform(paymentmethods: Paymentmethod[], filterQuery: string): Paymentmethod[] {
    filterQuery = filterQuery
      .trim()
      .toLowerCase();

    if (filterQuery) {
      return paymentmethods.filter(paymentmethod =>
        paymentmethod.nickName
          .toLowerCase()
          .concat(
            ' ',
            paymentmethod.fullName.toLowerCase()
          )
          .includes(filterQuery)
      );
    } else {
      return paymentmethods;
    }
  }

}
