import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Paymentmethod } from '../paymentmethod';
import { PaymentmethodsService } from '../paymentmethods.service';

@Component({
  selector: 'app-paymentmethods-list',
  templateUrl: './paymentmethods-list.component.html',
  styleUrls: ['./paymentmethods-list.component.css']
})
export class PaymentmethodsListComponent implements OnInit {

  paymentmethods: Paymentmethod[] = [];
  filter: string = '';
  hasMore: boolean = true;
  currentPage: number = 1;

  constructor(
    private activatedRoute: ActivatedRoute,
    private service: PaymentmethodsService
  ) { }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe(params => {
      this.paymentmethods = this.activatedRoute.snapshot.data['paymentmethods'];
    });
  }

  load() {

    this.service
      .listAllPaginated(++this.currentPage)
      .subscribe(paymentmethods => {
        this.filter = '';
        this.paymentmethods = this.paymentmethods.concat(paymentmethods);
        if (!paymentmethods.length) this.hasMore = false;
      });
  }

  remove(id: string) {
    this.service
      .remove(id)
      .subscribe(() => {
        this.paymentmethods = this.paymentmethods.filter((value) => {
          return value.nickName != id;
        });
      });
  }
}
