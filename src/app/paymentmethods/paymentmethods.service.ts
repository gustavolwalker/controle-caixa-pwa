import { Injectable } from '@angular/core';
import { IdbService } from '../core/idb/idb.service';
import { Observable } from 'rxjs';
import { PaymentmethodsDaoService } from './paymentmethods-dao.service';
import { Paymentmethod } from './paymentmethod';

@Injectable({
  providedIn: 'root'
})
export class PaymentmethodsService {
   
  constructor(
    private idbService: IdbService
  ) { }

  listAllPaginated(page: number) {

    return new Observable<Paymentmethod[]>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new PaymentmethodsDaoService(conn))
        .then(dao => dao.findAll(page))
        .then(paymentmethodss => {
          subscriber.next(paymentmethodss);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar formas de pagamento");
          subscriber.complete();
        });
    });
  }

  listAllSliced(size: number): Observable<[Paymentmethod[]]> {
  
    return new Observable<[Paymentmethod[]]>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new PaymentmethodsDaoService(conn))
        .then(dao => dao.findAll(null))
        .then(paymentmethods => {
          let parentArray: [Paymentmethod[]] = [[]];
          let childArray: Paymentmethod[] = [];
          paymentmethods.forEach((item: Paymentmethod, index) => {
            childArray.push(item);
            if (childArray.length === size || index === paymentmethods.length - 1) {
              parentArray.push(childArray);
              childArray = [];
            }
          });
          subscriber.next(parentArray);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar condições de pagamento");
          subscriber.complete();
        });
    });
  }


  get(id: string) {

    return new Observable<Paymentmethod>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new PaymentmethodsDaoService(conn))
        .then(dao => dao.findById(id))
        .then(paymentmethods => {
          subscriber.next(paymentmethods);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar forma de pagamento: " + id);
          subscriber.complete();
        });
    });
  }

  save(paymentmethod: Paymentmethod) {

    return new Observable(subscriber => {
      this.idbService.getConnection()
        .then(conn => new PaymentmethodsDaoService(conn))
        .then(dao => dao.update(paymentmethod))
        .then(() => {
          subscriber.next('Forma de pagamento editada com sucesso');
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao editar forma de pagamento");
          subscriber.complete();
        });
    });
  }

  remove(id: string) {

    return new Observable<Boolean>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new PaymentmethodsDaoService(conn))
        .then(dao => dao.delete(id))
        .then(() => {
          subscriber.next(true);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao remover forma de pagamento: " + id);
          subscriber.complete();
        });
    });
  }

  checkNickNameTaken(nickName: string) {

    if (nickName) 
      return this.idbService.getConnection()
        .then(conn => new PaymentmethodsDaoService(conn))
        .then(dao => dao.findById(nickName))            
  }
}
