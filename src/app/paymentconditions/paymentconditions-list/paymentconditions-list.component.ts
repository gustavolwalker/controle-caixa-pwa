import { Component, OnInit } from '@angular/core';
import { Paymentcondition } from '../paymentcondition';
import { ActivatedRoute } from '@angular/router';
import { PaymentconditionsService } from '../paymentconditions.service';

@Component({
  selector: 'app-paymentconditions-list',
  templateUrl: './paymentconditions-list.component.html',
  styleUrls: ['./paymentconditions-list.component.css']
})
export class PaymentconditionsListComponent implements OnInit {

  paymentconditions: Paymentcondition[] = [];
  filter: string = '';
  hasMore: boolean = true;
  currentPage: number = 1;

  constructor(
    private activatedRoute: ActivatedRoute,
    private service: PaymentconditionsService
  ) { }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe(params => {
      this.paymentconditions = this.activatedRoute.snapshot.data['paymentconditions'];
    });
  }

  load() {

    this.service
      .listAllPaginated(++this.currentPage)
      .subscribe(paymentconditions => {
        this.filter = '';
        this.paymentconditions = this.paymentconditions.concat(paymentconditions);
        if (!paymentconditions.length) this.hasMore = false;
      });
  }

  remove(id: string) {
    this.service
      .remove(id)
      .subscribe(() => {
        this.paymentconditions = this.paymentconditions.filter((value) => {
          return value.nickName != id;
        });
      });
  }
}