import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentconditionsListComponent } from './paymentconditions-list.component';

describe('PaymentconditionsListComponent', () => {
  let component: PaymentconditionsListComponent;
  let fixture: ComponentFixture<PaymentconditionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentconditionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentconditionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
