import { Pipe, PipeTransform } from '@angular/core';
import { Paymentcondition } from '../paymentcondition';

@Pipe({
  name: 'filterByNames'
})
export class FilterByNamesPipe implements PipeTransform {

  transform(paymentconditions: Paymentcondition[], filterQuery: string): Paymentcondition[] {
    filterQuery = filterQuery
      .trim()
      .toLowerCase();

    if (filterQuery) {
      return paymentconditions.filter(paymentcondition =>
        paymentcondition.nickName
          .toLowerCase()
          .concat(
            ' ',
            paymentcondition.fullName.toLowerCase()
          )
          .includes(filterQuery)
      );
    } else {
      return paymentconditions;
    }
  }

}
