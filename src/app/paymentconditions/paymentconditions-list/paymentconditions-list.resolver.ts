import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Paymentcondition } from '../paymentcondition';
import { PaymentconditionsService } from '../paymentconditions.service';

@Injectable({ providedIn: 'root' })
export class PaymentconditionsListResolver implements Resolve<Observable<Paymentcondition[]>>{

    constructor(private service: PaymentconditionsService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Paymentcondition[]> {        
        return this.service.listAllPaginated(1);
    }
}