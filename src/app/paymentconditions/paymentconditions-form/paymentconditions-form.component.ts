import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Paymentcondition } from '../paymentcondition';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PlatformDetectorService } from 'src/app/core/platform-detector/platform-detector.service';
import { PaymentconditionsService } from '../paymentconditions.service';
import { NickNameNotTakenValidatorService } from './nick-name-not-taken.validator.service';

@Component({
  selector: 'app-paymentconditions-form',
  templateUrl: './paymentconditions-form.component.html',
  styleUrls: ['./paymentconditions-form.component.css'],
  providers: [NickNameNotTakenValidatorService]
})
export class PaymentconditionsFormComponent implements OnInit {

  paymentcondition: Paymentcondition;
  paymentconditionForm: FormGroup;
  @ViewChild('nickNameInput', { static: true }) nickNameInput: ElementRef<HTMLInputElement>;
  @ViewChild('fullNameInput', { static: true }) fullNameInput: ElementRef<HTMLInputElement>;

  constructor(
    private formBuilder: FormBuilder,
    private nickNameNotTakenValidatorService: NickNameNotTakenValidatorService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private platformDetectorService: PlatformDetectorService,
    private service: PaymentconditionsService
  ) { }

  ngOnInit(): void {

    this.paymentconditionForm = this.formBuilder.group({
      nickName: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(16)
        ],
        this.nickNameNotTakenValidatorService.checkNickNameTaken()
      ],
      fullName: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(60)
        ]
      ],
      actived: [false]
    });

    this.platformDetectorService.isPlatformBrowser() &&
      this.nickNameInput.nativeElement.focus();

    this.activatedRoute.params
      .subscribe((params: Params) => {
        if (params && params.id) {
          let id: string = params.id;
          console.log(id);
          this.service.get(id)
            .subscribe(paymentcondition => {
              this.paymentcondition = paymentcondition;

              console.log(this.paymentcondition)
              if (this.paymentcondition) {
                this.paymentconditionForm.patchValue({
                  nickName: this.paymentcondition.nickName,
                  fullName: this.paymentcondition.fullName,
                  actived: this.paymentcondition.actived
                });
                console.log("alimentou form builder")
                const nicknameControl = this.paymentconditionForm.get('nickName');
                console.log("confere nickname para foco")
                if (this.paymentcondition.nickName) {
                  nicknameControl.disable();
                  this.platformDetectorService.isPlatformBrowser() &&
                    this.fullNameInput.nativeElement.focus();
                }
                console.log("ajustou o foco")

              }
            })
        }
      });
  }

  save() {

    if (this.paymentconditionForm.valid && !this.paymentconditionForm.pending) {
      const paymentcondition = this.paymentconditionForm.getRawValue() as Paymentcondition;
      this.service
        .save(paymentcondition)
        .subscribe(
          () => this.router.navigate(['condicaodepagamento']),
          err => console.log(err)
        );
    }
  }
}