import { Injectable } from '@angular/core';
import { PaymentconditionsService } from '../paymentconditions.service';
import { AbstractControl } from '@angular/forms';
import { debounceTime, switchMap, map, tap, first } from 'rxjs/operators';

@Injectable()
export class NickNameNotTakenValidatorService {

  constructor(private service: PaymentconditionsService) {}

  checkNickNameTaken() {

      return (control: AbstractControl) => {
          return control
              .valueChanges
              .pipe(debounceTime(300))
              .pipe(switchMap(nickName => 
                      this.service.checkNickNameTaken(nickName)
              ))
              .pipe(map(isTaken => isTaken ? { nickNameTaken: true } : null))
              .pipe(tap(r => console.log(r)))
              .pipe(first());
      }
  }
}