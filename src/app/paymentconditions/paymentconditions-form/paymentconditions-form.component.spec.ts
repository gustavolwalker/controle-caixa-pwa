import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentconditionsFormComponent } from './paymentconditions-form.component';

describe('PaymentconditionsFormComponent', () => {
  let component: PaymentconditionsFormComponent;
  let fixture: ComponentFixture<PaymentconditionsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentconditionsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentconditionsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
