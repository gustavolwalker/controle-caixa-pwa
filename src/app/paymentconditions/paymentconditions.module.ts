import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptor } from '../core/auth/request.interceptor';
import { PaymentconditionsListComponent } from './paymentconditions-list/paymentconditions-list.component';
import { PaymentconditionsFormComponent } from './paymentconditions-form/paymentconditions-form.component';
import { ActivedModule } from '../shared/pipes/actived/actived.module';
import { BackButtonModule } from '../shared/directives/back-button/back-button.module';
import { FilterByNamesPipe } from './paymentconditions-list/filter-by-names.pipe';
import { FilterListModule } from '../shared/components/filter-list/filter-list.module';
import { VMessageModule } from '../shared/components/vmessage/vmessage.module';

@NgModule({
  declarations: [
    PaymentconditionsListComponent, 
    PaymentconditionsFormComponent,
    FilterByNamesPipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ActivedModule,
    BackButtonModule,    
    FilterListModule,
    VMessageModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    }
  ]
})
export class PaymentconditionsModule { }
