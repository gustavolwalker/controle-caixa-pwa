import { TestBed } from '@angular/core/testing';

import { PaymentconditionsService } from './paymentconditions.service';

describe('PaymentconditionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentconditionsService = TestBed.get(PaymentconditionsService);
    expect(service).toBeTruthy();
  });
});
