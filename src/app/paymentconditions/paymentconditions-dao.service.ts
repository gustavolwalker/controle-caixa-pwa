import { Inject } from '@angular/core';
import { Paymentcondition } from './paymentcondition';

@Inject({
  providedIn: 'root'
})
export class PaymentconditionsDaoService {

  private store = 'paymentconditions';
  constructor(private connection) { }

  insert(paymentcondition: Paymentcondition) {

    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .add(paymentcondition);

      request.onsuccess = e => {

        resolve();
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject('Não foi possível inserir a condição de pagamento');

      };
    });
  }

  update(paymentcondition: Paymentcondition) {

    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .put(paymentcondition);

      request.onsuccess = e => {

        resolve();
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject('Não foi possível atualizar a condição de pagamento');

      };
    });
  }

  delete(id: string) {
    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .delete(id);

      request.onsuccess = e => {

        resolve();
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject('Não foi possível excluir a condição de pagamento');

      };
    });
  }

  findById(id: string) {

    return new Promise<Paymentcondition>((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readonly')
        .objectStore(this.store)
        .get(id);

      request.onsuccess = e => {

        console.log(`Dao get ${id} result ${e.target.result}`);
        resolve(e.target.result);
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject(`Não foi possível encontrar a condição de pagamento: ${id}`);
      };
    });
  }

  findAll(page: number) {
    return new Promise<Paymentcondition[]>((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readonly')
        .objectStore(this.store)
        .getAll();

      request.onsuccess = e => {

        resolve(e.target.result);
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject(`Não foi possível buscar as condições de pagamento`);
      };
    });
  }

}
