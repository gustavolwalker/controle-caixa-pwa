import { Injectable } from '@angular/core';
import { IdbService } from '../core/idb/idb.service';
import { Observable } from 'rxjs';
import { Paymentcondition } from './paymentcondition';
import { PaymentconditionsDaoService } from './paymentconditions-dao.service';

@Injectable({
  providedIn: 'root'
})
export class PaymentconditionsService {

  constructor(
    private idbService: IdbService
  ) { }

  listAllPaginated(page: number): Observable<Paymentcondition[]> {

    return new Observable<Paymentcondition[]>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new PaymentconditionsDaoService(conn))
        .then(dao => dao.findAll(page))
        .then(paymentconditionss => {
          subscriber.next(paymentconditionss);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar condições de pagamento");
          subscriber.complete();
        });
    });
  }

  listAllSliced(size: number): Observable<[Paymentcondition[]]> {
    
    return new Observable<[Paymentcondition[]]>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new PaymentconditionsDaoService(conn))
        .then(dao => dao.findAll(null))
        .then(paymentconditions => {
          let parentArray: [Paymentcondition[]] = [[]];
          let childArray: Paymentcondition[] = [];
          paymentconditions.forEach((item: Paymentcondition, index) => {
            childArray.push(item);
            if (childArray.length === size || index === paymentconditions.length - 1) {
              parentArray.push(childArray);
              childArray = [];
            }
          });
          subscriber.next(parentArray);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar condições de pagamento");
          subscriber.complete();
        });
    });
  }

  get(id: string) {

    return new Observable<Paymentcondition>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new PaymentconditionsDaoService(conn))
        .then(dao => dao.findById(id))
        .then(paymentconditions => {
          subscriber.next(paymentconditions);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar condição de pagamento: " + id);
          subscriber.complete();
        });
    });
  }

  save(paymentcondition: Paymentcondition) {
    return new Observable(subscriber => {
      this.idbService.getConnection()
        .then(conn => new PaymentconditionsDaoService(conn))
        .then(dao => dao.update(paymentcondition))
        .then(() => {
          subscriber.next('Condição de pagamento editada com sucesso');
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao editar condição de pagamento");
          subscriber.complete();
        });
    });
  }

  remove(id: string) {
    return new Observable<Boolean>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new PaymentconditionsDaoService(conn))
        .then(dao => dao.delete(id))
        .then(() => {
          subscriber.next(true);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao remover condição de pagamento: " + id);
          subscriber.complete();
        });
    });
  }

  checkNickNameTaken(nickName: string) {

    if (nickName)
      return this.idbService.getConnection()
        .then(conn => new PaymentconditionsDaoService(conn))
        .then(dao => dao.findById(nickName))
  }
}
