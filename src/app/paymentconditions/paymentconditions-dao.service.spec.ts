import { TestBed } from '@angular/core/testing';

import { PaymentconditionsDaoService } from './paymentconditions-dao.service';

describe('PaymentconditionsDaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentconditionsDaoService = TestBed.get(PaymentconditionsDaoService);
    expect(service).toBeTruthy();
  });
});
