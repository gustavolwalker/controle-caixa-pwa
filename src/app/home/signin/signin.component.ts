import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../core/auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PlatformDetectorService } from '../../core/platform-detector/platform-detector.service';
import { Observable } from 'rxjs';

@Component({
    templateUrl: './signin.component.html'
})
export class SignInComponent implements OnInit {

    fromUrl: string;
    loginForm: FormGroup;
    @ViewChild('emailInput', { static: true }) emailInput: ElementRef<HTMLInputElement>;
    signinVMsg$: Observable<string>;

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router,
        private platformDetectorService: PlatformDetectorService,
        private activatedRoute: ActivatedRoute) { }

    ngOnInit(): void {
        this.activatedRoute
            .queryParams
            .subscribe(params => this.fromUrl = params['fromUrl']);

        this.loginForm = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
        this.platformDetectorService.isPlatformBrowser() &&
            this.emailInput.nativeElement.focus();
    }

    login() {

        const val = this.loginForm.value;

        if (val.email && val.password) {
            this.authService
                .authenticate(val.email, val.password)
                .subscribe(
                    () => {
                        this.fromUrl
                            ? this.router.navigateByUrl(this.fromUrl)
                            : this.router.navigateByUrl('dashboard');
                    },
                    err => {
                        console.log(err);
                        this.signinVMsg$ = err;
                        this.loginForm.reset();                        
                        this.platformDetectorService.isPlatformBrowser() &&
                            this.emailInput.nativeElement.focus();
                    
                    }
                );
        }
    }
}