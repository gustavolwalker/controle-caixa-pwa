import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NewUser } from './new-user';

import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { IdbService } from 'src/app/core/idb/idb.service';
import { UserDaoService } from 'src/app/core/user/user-dao.service';

const API = environment.ApiUrl;

@Injectable()
export class SignUpService {

    constructor(private http: HttpClient, private idbService: IdbService) { }

    checkEmailTaken(userName: string) {

        if (userName)
            return this.idbService.getConnection()
                .then(conn => new UserDaoService(conn))
                .then(dao => dao.findById(userName));
    }

    signup(newUser: NewUser) {
        
        if (newUser) {
            //return this.http.post(API + '/user/signup', newUser);
            return new Observable(subscriber => {
                this.idbService.getConnection()
                    .then(conn => new UserDaoService(conn))
                    .then(dao => dao.insert(newUser))
                    .then(() => {
                        subscriber.next('Usuário adicionado com sucesso');
                        subscriber.complete();
                    })
                    .catch(erro => {
                        console.log(erro);
                        throw new Error('Não foi possível adicionar o usuário')
                    });
            });
        }
    }
}