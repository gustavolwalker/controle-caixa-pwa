import { ValidatorFn, FormGroup } from "@angular/forms";

export const emailPassword: ValidatorFn = (formGroup: FormGroup) => {
    const email = formGroup.get('email').value;
    const password = formGroup.get('password').value;

    if(email.trim() + password.trim()) {
        return email != password 
        ? null 
        : { emailPassword: true };
    } else {
        return null;
    }
};