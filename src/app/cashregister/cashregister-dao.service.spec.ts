import { TestBed } from '@angular/core/testing';

import { CashregisterDaoService } from './cashregister-dao.service';

describe('CashregisterDaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CashregisterDaoService = TestBed.get(CashregisterDaoService);
    expect(service).toBeTruthy();
  });
});
