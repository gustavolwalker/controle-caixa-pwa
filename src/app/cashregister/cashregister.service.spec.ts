import { TestBed } from '@angular/core/testing';

import { CashregisterService } from './cashregister.service';

describe('CashregisterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CashregisterService = TestBed.get(CashregisterService);
    expect(service).toBeTruthy();
  });
});
