import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Cashregister } from '../cashregister';
import { ActivatedRoute } from '@angular/router';
import { CashregisterService } from '../cashregister.service';

@Component({
  selector: 'app-cashregister-list',
  templateUrl: './cashregister-list.component.html',
  styleUrls: ['./cashregister-list.component.css']
})
export class CashregisterListComponent implements OnInit {

  @ViewChild('pesquisarInput', { static: false }) pesquisarInput: ElementRef<HTMLInputElement>;
  cashregister: Cashregister[] = [];
  filter: string = '';
  hasMore: boolean = true;
  currentPage: number = 1;

  constructor(
    private activatedRoute: ActivatedRoute,
    private service: CashregisterService
  ) { }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe(() => {
      this.cashregister = this.activatedRoute.snapshot.data['cashregister'];
    });
  }

  pesquisar() {

    console.log("vou pesquisar: " + this.pesquisarInput.nativeElement.value);
  }

  load() {

    this.service
      .listAllPaginated(++this.currentPage)
      .subscribe(cashregister => {
        this.filter = '';
        this.cashregister = this.cashregister.concat(cashregister);
        if (!cashregister.length) this.hasMore = false;
      });
  }

  remove(id: number) {
    
    this.service
      .remove(id)
      .subscribe(() => {
        this.cashregister = this.cashregister.filter((value) => {
          return value.id != id;
        });
      });
  }
}
