import { Pipe, PipeTransform } from '@angular/core';
import { Cashregister } from '../cashregister';

@Pipe({
  name: 'filterByIdNames'
})
export class FilterByIdNamesPipe implements PipeTransform {

  transform(cashregister: Cashregister[], filterQuery: string): Cashregister[] {
    filterQuery = filterQuery
      .trim()
      .toLowerCase();

    if (filterQuery) {
      return cashregister.filter(cashregister =>
        cashregister.id.toString()
          .toLowerCase()
          .concat(
            ' ',
            cashregister.nickName.toLowerCase(),            
          )
          .includes(filterQuery)
      );
    } else {
      return cashregister;
    }
  }

}
