import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashregisterListComponent } from './cashregister-list.component';

describe('CashregisterListComponent', () => {
  let component: CashregisterListComponent;
  let fixture: ComponentFixture<CashregisterListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashregisterListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashregisterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
