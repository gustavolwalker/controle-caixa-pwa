import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Cashregister } from '../cashregister';
import { CashregisterService } from '../cashregister.service';

@Injectable({ providedIn: 'root' })
export class CashregisterListResolver implements Resolve<Observable<Cashregister[]>>{

    constructor(private service: CashregisterService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Cashregister[]> {
        console.log("Passei no resolver");
        return this.service.listAllPaginated(1);
    }
}