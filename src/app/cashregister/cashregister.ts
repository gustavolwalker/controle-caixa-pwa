import { Paymentmethod } from '../paymentmethods/paymentmethod';
import { Paymentcondition } from '../paymentconditions/paymentcondition';

export interface Cashregister {
    id: number,
    nickName: string,
    defaultPaymentMethod: Paymentmethod,
    defaultPaymentCondition: Paymentcondition,
    actived: boolean
}