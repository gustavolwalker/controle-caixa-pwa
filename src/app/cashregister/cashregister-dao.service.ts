import { Inject } from '@angular/core';
import { Cashregister } from './cashregister';

@Inject({
  providedIn: 'root'
})
export class CashregisterDaoService {

  private store = 'cashregister';
  constructor(private connection) { }

  insert(cashregister: Cashregister) {

    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .add(cashregister);

      request.onsuccess = e => {

        resolve();
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject('Não foi possível inserir o caixa');

      };
    });
  }

  update(cashregister: Cashregister) {

    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .put(cashregister);

      request.onsuccess = e => {

        resolve();
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject('Não foi possível atualizar o caixa');

      };
    });
  }

  delete(id: number) {

    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .delete(id);

      request.onsuccess = e => {

        resolve();
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject('Não foi possível excluir o caixa');

      };
    });
  }

  findById(id: number) {

    return new Promise<Cashregister>((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readonly')
        .objectStore(this.store)
        .get(id);

      request.onsuccess = e => {

        console.log(`Dao get ${id} result ${e.target.result}`);
        resolve(e.target.result);
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject(`Não foi possível encontrar o caixa: ${id}`);
      };
    });
  }

  findAll(page: number) {

    return new Promise<Cashregister[]>((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readonly')
        .objectStore(this.store)
        .getAll();

      request.onsuccess = e => {

        resolve(e.target.result);
      };

      request.onerror = e => {

        console.log(e.target.error);
        reject(`Não foi possível buscar os caixas`);
      };
    });
  }
}
