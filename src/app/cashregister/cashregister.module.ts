import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptor } from '../core/auth/request.interceptor';
import { ActivedModule } from '../shared/pipes/actived/actived.module';
import { BackButtonModule } from '../shared/directives/back-button/back-button.module';
import { FilterListModule } from '../shared/components/filter-list/filter-list.module';
import { VMessageModule } from '../shared/components/vmessage/vmessage.module';
import { CashregisterListComponent } from './cashregister-list/cashregister-list.component';
import { FilterByIdNamesPipe } from './cashregister-list/filter-by-id-names.pipe';
import { CashregisterFormComponent } from './cashregister-form/cashregister-form.component';

@NgModule({
  declarations: [
    CashregisterListComponent,
    FilterByIdNamesPipe,
    CashregisterFormComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ActivedModule,
    BackButtonModule,
    FilterListModule,
    VMessageModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    }
  ]
})
export class CashregisterModule { }
