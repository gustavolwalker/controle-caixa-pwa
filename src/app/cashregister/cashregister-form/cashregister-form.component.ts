import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Cashregister } from '../cashregister';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PlatformDetectorService } from 'src/app/core/platform-detector/platform-detector.service';
import { CashregisterService } from '../cashregister.service';
import { Paymentmethod } from 'src/app/paymentmethods/paymentmethod';
import { Paymentcondition } from 'src/app/paymentconditions/paymentcondition';

@Component({
    selector: 'app-cashregister-form',
    templateUrl: './cashregister-form.component.html',
    styleUrls: ['./cashregister-form.component.css']
})
export class CashregisterFormComponent implements OnInit {

    cashregister: Cashregister;
    cashregisterForm: FormGroup;
    methods: Paymentmethod[];
    conditions: Paymentcondition[];
    @ViewChild('nickNameInput', { static: true }) nickNameInput: ElementRef<HTMLInputElement>;
    @ViewChild('methodSelect', { static: true }) methodSelect: ElementRef<HTMLSelectElement>;
    @ViewChild('conditionSelect', { static: true }) conditionSelect: ElementRef<HTMLSelectElement>;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private platformDetectorService: PlatformDetectorService,
        private service: CashregisterService
    ) { }

    ngOnInit(): void {

        this.cashregisterForm = this.formBuilder.group({
            id: [null],
            nickName: ['',
                [
                    Validators.required,
                    Validators.minLength(2),
                    Validators.maxLength(8)
                ]
            ],
            defaultPaymentMethod: [
                null,
                [
                    Validators.required,
                ]
            ],
            defaultPaymentCondition: [
                null,
                [
                    Validators.required,
                ]
            ],
            actived: [false]
        });

        this.activatedRoute.params.subscribe((params: Params) => {

            if (this.activatedRoute.snapshot.data['paymentmethods'])
                this.methods = this.activatedRoute.snapshot.data['paymentmethods'];
            if (this.activatedRoute.snapshot.data['paymentconditions'])
                this.conditions = this.activatedRoute.snapshot.data['paymentconditions'];

            if (params && params.id) {
                let id: number = +params.id;
                this.service.get(id).subscribe(cashregister => {
                    this.cashregister = cashregister;
                    if (this.cashregister) {
                        console.log(this.cashregister.defaultPaymentMethod)
                        this.cashregisterForm.patchValue({
                            id: this.cashregister.id,
                            nickName: this.cashregister.nickName,
                            defaultPaymentMethod:
                                this.methods.find(
                                    (element) => element.nickName === this.cashregister.defaultPaymentMethod.nickName
                                ),
                            defaultPaymentCondition:
                                this.conditions.find(
                                    (element) => element.nickName === this.cashregister.defaultPaymentCondition.nickName
                                ),
                            actived: this.cashregister.actived
                        });
                    }
                })
            }
        });

        this.platformDetectorService.isPlatformBrowser() &&
            this.nickNameInput.nativeElement.focus();
    }

    save() {

        if (this.cashregisterForm.valid && !this.cashregisterForm.pending) {
            const cashregister = this.cashregisterForm.getRawValue() as Cashregister;
            this.service
                .save(cashregister)
                .subscribe(
                    () => this.router.navigate(['caixa']),
                    err => console.log(err)
                );
        }
    }

}
