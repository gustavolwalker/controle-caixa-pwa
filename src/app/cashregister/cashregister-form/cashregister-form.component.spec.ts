import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashregisterFormComponent } from './cashregister-form.component';

describe('CashregisterFormComponent', () => {
  let component: CashregisterFormComponent;
  let fixture: ComponentFixture<CashregisterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashregisterFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashregisterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
