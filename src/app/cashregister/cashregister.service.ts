import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IdbService } from '../core/idb/idb.service';
import { Cashregister } from './cashregister';
import { CashregisterDaoService } from './cashregister-dao.service';

@Injectable({
  providedIn: 'root'
})
export class CashregisterService {

  constructor(
    private idbService: IdbService
  ) { }

  listAllPaginated(page: number) {

    return new Observable<Cashregister[]>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new CashregisterDaoService(conn))
        .then(dao => dao.findAll(page))
        .then(cashregister => {
          subscriber.next(cashregister);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar caixas");
          subscriber.complete();
        });
    });
  }

  get(id: number) {

    return new Observable<Cashregister>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new CashregisterDaoService(conn))
        .then(dao => dao.findById(id))
        .then(cashregister => {
          subscriber.next(cashregister);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar caixa: " + id);
          subscriber.complete();
        });
    });
  }

  save(cashregister: Cashregister) {
    
    if (!cashregister.id || cashregister.id == 0) {
      delete cashregister.id;
      return new Observable(subscriber => {
        this.idbService.getConnection()
          .then(conn => new CashregisterDaoService(conn))
          .then(dao => dao.insert(cashregister))
          .then(() => {
            subscriber.next('Caixa adicionado com sucesso');
            subscriber.complete();
          })
          .catch(erro => {
            console.log(erro);
            subscriber.error("Erro ao inserir caixa");
            subscriber.complete();
          });
      });
    } else {      
      return new Observable(subscriber => {
        this.idbService.getConnection()
          .then(conn => new CashregisterDaoService(conn))
          .then(dao => dao.update(cashregister))
          .then(() => {
            subscriber.next('Caixa editado com sucesso');
            subscriber.complete();
          })
          .catch(erro => {
            console.log(erro);
            subscriber.error("Erro ao editar caixa");
            subscriber.complete();
          });
      });
    }
  }

  remove(id: number) {
    
    return new Observable<Boolean>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new CashregisterDaoService(conn))
        .then(dao => dao.delete(id))
        .then(() => {
          subscriber.next(true);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao remover caixa: " + id);
          subscriber.complete();
        });
    });
  }
}
