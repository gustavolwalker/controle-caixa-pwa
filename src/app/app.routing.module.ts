import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';
import { PaymentconditionsListComponent } from './paymentconditions/paymentconditions-list/paymentconditions-list.component';
import { PaymentconditionsListResolver } from './paymentconditions/paymentconditions-list/paymentconditions-list.resolver';
import { PaymentconditionsFormComponent } from './paymentconditions/paymentconditions-form/paymentconditions-form.component';
import { PaymentmethodsListComponent } from './paymentmethods/paymentmethods-list/paymentmethods-list.component';
import { PaymentmethodsListResolver } from './paymentmethods/paymentmethods-list/paymentmethods-list.resolver';
import { PaymentmethodsFormComponent } from './paymentmethods/paymentmethods-form/paymentmethods-form.component';
import { SalesmansListComponent } from './salesmans/salesmans-list/salesmans-list.component';
import { SalesmansListResolver } from './salesmans/salesmans-list/salesmans-list.resolver';
import { SalesmansFormComponent } from './salesmans/salesmans-form/salesmans-form.component';
import { GlobalErrorComponent } from './errors/global-error/global-error.component';
import { NotFoundComponent } from './errors/not-found/not-found.component';
import { CashregisterListComponent } from './cashregister/cashregister-list/cashregister-list.component';
import { CashregisterListResolver } from './cashregister/cashregister-list/cashregister-list.resolver';
import { CashregisterFormComponent } from './cashregister/cashregister-form/cashregister-form.component';
import { DashBalanceComponent } from './dashboard/dash-balance/dash-balance.component';
import { DailyregistersDashComponent } from './dailyregisters/dailyregisters-dash/dailyregisters-dash.component';
import { DailyregistersEntryComponent } from './dailyregisters/dailyregisters-entry/dailyregisters-entry.component';
import { DailyregistersEntryResolver } from './dailyregisters/dailyregisters-entry/dailyregisters-entry.resolver';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home',
    },
    {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
    },
    {
        path: 'caixa',
        pathMatch: 'full',
        component: CashregisterListComponent,
        resolve: {
            cashregister: CashregisterListResolver
        },
        canActivate: [AuthGuard],
        data: {
            title: 'Caixa'
        }
    },
    {
        path: 'caixa/add',
        component: CashregisterFormComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Caixas - Adição'
        }
    },
    {
        path: 'caixa/:id',
        component: CashregisterFormComponent,
        canActivate: [AuthGuard],
        resolve: {
            paymentmethods: PaymentmethodsListResolver,
            paymentconditions: PaymentconditionsListResolver
        },
        data: {
            title: 'Caixas - Edição'
        }
    },
    {
        path: 'dashboard',
        pathMatch: 'full',
        component: DashBalanceComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Dashboard'
        }
    },
    {
        path: 'diario',
        pathMatch: 'full',
        component: DailyregistersDashComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Controle Diário'
        }
    },
    {
        path: 'diario/:id',
        component: DailyregistersEntryComponent,
        resolve: {
            dailyregister: DailyregistersEntryResolver,
            salesmans: SalesmansListResolver,
            paymentmethods: PaymentmethodsListResolver,
            paymentconditions: PaymentconditionsListResolver
        },
        canActivate: [AuthGuard],
        data: {
            title: 'Controle Diário - Vendas'
        }
    },
    {
        path: 'condicaodepagamento',
        pathMatch: 'full',
        component: PaymentconditionsListComponent,
        resolve: {
            paymentconditions: PaymentconditionsListResolver
        },
        canActivate: [AuthGuard],
        data: {
            title: 'Condições de pagamento'
        }
    },
    {
        path: 'condicaodepagamento/add',
        component: PaymentconditionsFormComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Condições de pagamento - Adição'
        }
    },
    {
        path: 'condicaodepagamento/:id',
        component: PaymentconditionsFormComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Condições de pagamento - Edição'
        }
    },
    {
        path: 'formadepagamento',
        pathMatch: 'full',
        component: PaymentmethodsListComponent,
        resolve: {
            paymentmethods: PaymentmethodsListResolver
        },
        canActivate: [AuthGuard],
        data: {
            title: 'Formas de pagamento'
        }
    },
    {
        path: 'formadepagamento/add',
        component: PaymentmethodsFormComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Formas de pagamento - Adição'
        }
    },
    {
        path: 'formadepagamento/:id',
        component: PaymentmethodsFormComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Formas de pagamento - Edição'
        }
    },
    {
        path: 'vendedor',
        pathMatch: 'full',
        component: SalesmansListComponent,
        resolve: {
            salesmans: SalesmansListResolver
        },
        canActivate: [AuthGuard],
        data: {
            title: 'Vendedores'
        }
    },
    {
        path: 'vendedor/add',
        component: SalesmansFormComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Vendedores - Adição'
        }
    },
    {
        path: 'vendedor/:id',
        component: SalesmansFormComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Vendedores - Edição'
        }
    },
    {
        path: 'error',
        component: GlobalErrorComponent,
        data: {
            title: 'Error'
        }
    },
    {
        path: 'not-found',
        component: NotFoundComponent,
        data: {
            title: 'Not found'
        }
    },
    {
        path: '**',
        redirectTo: 'not-found'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { useHash: false })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }

