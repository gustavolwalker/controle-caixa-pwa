import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { UserService } from '../user/user.service';
import { UserDaoService } from '../user/user-dao.service';
import { Observable } from 'rxjs';
import { IdbService } from '../idb/idb.service';

const API_URL = 'http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private idbService: IdbService) { }

  /*authenticate(userName: string, password: string) {

    return this.http
      .post(
        API_URL + '/user/login', 
        { userName, password }, 
        { observe: 'response'} 
      )
      .pipe(tap(res => {
        const authToken = res.headers.get('x-access-token');
        this.userService.setToken(authToken);
        console.log(`User ${userName} authenticated with token ${authToken}`);
      }));
  }*/

  authenticate(email: string, password: string) {

    return new Observable(subscriber => {
      this.idbService.getConnection()
        .then(conn => new UserDaoService(conn))
        .then(dao => dao.validateUser(email, password))
        .then(user => {
          let authToken = JSON.stringify(user);
          this.userService.setToken(authToken);
          console.log(`User ${email} authenticated with token ${authToken}`);
          subscriber.next('Usuário logado com sucesso');
          subscriber.complete();
        })
        .catch(erro => {          
          console.log(erro);
          subscriber.error("E-mail e ou Senha inválidos");
          subscriber.complete();          
        });
    });
  }


}