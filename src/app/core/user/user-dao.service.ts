import { Inject } from '@angular/core';
import { Md5 } from 'ts-md5/dist/md5';
import { User } from './user';
import { NewUser } from 'src/app/home/singup/new-user';

@Inject({
  providedIn: 'root'
})
export class UserDaoService {

  private store = 'users';
  constructor(private connection: IDBDatabase) {  }

  insert(user: NewUser) {

    return new Promise((resolve, reject) => {

      user.password = Md5.hashStr(user.password) as string;

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .add(user);

      request.onsuccess = (e: Event) => {

        resolve();
      };

      request.onerror = () => {

        console.log(request.error);
        reject('Não foi possível adicionar o usuário');

      };
    });
  }

  findById(id: string) {

    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readonly')
        .objectStore(this.store)
        .get(id);

      request.onsuccess = () => {

        resolve(request.result);
      };

      request.onerror = () => {

        console.log(request.error);
        reject(`Não foi possível encontrar o usuário: ${id}`);
      };
    });
  }

  validateUser(email: string, password: string): any {

    return new Promise((resolve, reject) => {
      this.findById(email)
        .then(
          (user: NewUser) => {
            if (!!user && user.password == Md5.hashStr(password))
              resolve(user as User);
            else
              reject(`E-mail e ou Senha inválidos`);
          }
        ).catch(erro => {
          console.log(erro);
          throw new Error('Não foi possível validar o usuário')
        });
    });
  }
}
