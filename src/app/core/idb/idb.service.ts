import { Injectable } from '@angular/core';
import { Paymentmethod } from 'src/app/paymentmethods/paymentmethod';
import { Paymentcondition } from 'src/app/paymentconditions/paymentcondition';

@Injectable({
  providedIn: 'root'
})
export class IdbService {

  private dbName = 'controle-caixa';
  private dbVersion = 1;
  private dbConnection = null;
  private close = null;

  getConnection(): Promise<IDBDatabase> {

    return new Promise((resolve, reject) => {

      let openRequest = window.indexedDB.open(this.dbName, this.dbVersion);

      openRequest.onupgradeneeded = (e: any) => {

        this.createStores(e.target.result);
      };

      openRequest.onsuccess = (e: any) => {

        if (!this.dbConnection) {
          this.dbConnection = e.target.result;
          this.close = this.dbConnection.close.bind(this.dbConnection);
          this.dbConnection.close = function () {
            throw new Error('Você não pode fechar diretamente a conexão');
          };
        }
        resolve(this.dbConnection);

      };

      openRequest.onerror = (e: any) => {

        console.log(e.target.error);
        reject(e.target.error.name);
      };
    });
  }

  private createStores(connection) {

    console.log(`Upgrading to version ${connection.version}`);

    if (!connection.objectStoreNames.contains('users'))
      connection.createObjectStore('users', { keyPath: 'email' });

    if (!connection.objectStoreNames.contains('salesmans')) {
      connection.createObjectStore('salesmans', { keyPath: 'id', autoIncrement: true })
        .add({
          id: 1,
          fullName: 'VENDEDOR 1',
          nickName: 'VEND1',
          actived: true
        });
    }

    const paymentMethod: Paymentmethod = {
      nickName: 'DINHEIRO',
      fullName: 'DINHEIRO',
      actived: true
    }

    if (!connection.objectStoreNames.contains('paymentmethods')) {
      connection.createObjectStore('paymentmethods', { keyPath: 'nickName' })
        .add(paymentMethod);
    }

    const paymentCondition: Paymentcondition = {
      nickName: 'AVISTA',
      fullName: 'AVISTA',
      actived: true
    }
    if (!connection.objectStoreNames.contains('paymentconditions')) {
      connection.createObjectStore('paymentconditions', { keyPath: 'nickName' })
        .add(paymentCondition);
    }

    if (!connection.objectStoreNames.contains('cashregister')) {
      connection.createObjectStore('cashregister', { keyPath: 'id', autoIncrement: true })
        .add({
          id: 1,
          nickName: "CAIXA",
          defaultPaymentMethod: paymentMethod.nickName,
          defaultPaymentCondition: paymentCondition.nickName,
          actived: true
        });
    }

    if (!connection.objectStoreNames.contains('dailyregisters')) {
      var objectStore = connection.createObjectStore('dailyregisters', { keyPath: 'id', autoIncrement: true });
      objectStore.createIndex("idxDate", "date", { unique: true });
    }
  }

  closeConnection() {

    if (this.dbConnection) {
      close();
      this.dbConnection = null;
      this.close = null;
    }
  }
}
