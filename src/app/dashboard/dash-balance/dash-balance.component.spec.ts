import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashBalanceComponent } from './dash-balance.component';

describe('DashBalanceComponent', () => {
  let component: DashBalanceComponent;
  let fixture: ComponentFixture<DashBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
