import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashBalanceComponent } from './dash-balance/dash-balance.component';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptor } from '../core/auth/request.interceptor';

@NgModule({
  declarations: [DashBalanceComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    }
  ]
})
export class DashboardModule { }
