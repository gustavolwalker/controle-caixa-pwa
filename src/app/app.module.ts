import { NgModule, LOCALE_ID } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app.routing.module';
import { CoreModule } from './core/core.module';
import { ErrorsModule } from './errors/errors.module';
import { PaymentconditionsModule } from './paymentconditions/paymentconditions.module';
import { PaymentmethodsModule } from './paymentmethods/paymentmethods.module';
import { SalesmansModule } from './salesmans/salesmans.module';
import { CashregisterModule } from './cashregister/cashregister.module';
import { DailyregistersModule } from './dailyregisters/dailyregisters.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { registerLocaleData } from '@angular/common';
import ptBr from '@angular/common/locales/pt';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
registerLocaleData(ptBr)

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule,
    CoreModule,
    ErrorsModule,
    CashregisterModule,
    DailyregistersModule,
    DashboardModule,
    PaymentconditionsModule,
    PaymentmethodsModule,
    SalesmansModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: navigator.language
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }