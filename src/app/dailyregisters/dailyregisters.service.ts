import { Injectable } from '@angular/core';
import { IdbService } from '../core/idb/idb.service';
import { Observable } from 'rxjs';
import { Dailyregister } from './dailyregister';
import { DailyregistersDaoService } from './dailyregisters-dao.service';
import { SortDirection } from '../shared/directives/sortable/sortable.directive';
import { DailyregisterEntry } from './dailyregister-entry';
import { CashregisterService } from '../cashregister/cashregister.service';

function compare(v1, v2) {

  return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sortEntries(entries: DailyregisterEntry[], column: string, direction: string): DailyregisterEntry[] {

  if (direction === '') {
    return entries;
  } else {
    return [...entries].sort((a, b) => {
      let res;
      if (column == 'value') {
        res = compare(
          (a[column] as number * (a.credit ? 1 : -1)),
          (b[column] as number * (b.credit ? 1 : -1))
        );
      } else if (column.indexOf('.') > -1) {
        res = compare(a[column.split('.')[0]][column.split('.')[1]], b[column.split('.')[0]][column.split('.')[1]]);
      } else {
        res = compare(a[column], b[column]);
      }
      return direction === 'asc' ? res : -res;
    });
  }
}


@Injectable({
  providedIn: 'root'
})
export class DailyregistersService {

  public sortColumn: string = 'id';
  public sortDirection: SortDirection = 'desc';


  constructor(
    private idbService: IdbService,
    private cashregisterService: CashregisterService
  ) { }


  listAllPaginated(page: number) {

    return new Observable<Dailyregister[]>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new DailyregistersDaoService(conn))
        .then(dao => dao.findAll(page))
        .then(dailyregisters => {
          subscriber.next(dailyregisters);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar registros diários");
          subscriber.complete();
        });
    });
  }

  listAllEntries(dailyregister: Dailyregister) {
    if (!dailyregister.entries || dailyregister.entries.length == 0)
      return dailyregister.entries;
    else
      return sortEntries(dailyregister.entries, this.sortColumn, this.sortDirection);
  }

  get(id: number) {

    return new Observable<Dailyregister>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new DailyregistersDaoService(conn))
        .then(dao => dao.findById(id))
        .then(dailyregister => {
          subscriber.next(dailyregister);
          subscriber.complete();
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar registro diário: " + id);
          subscriber.complete();
        });
    });
  }

  openDate(date: Date) {
    return new Observable<Dailyregister>(subscriber => {
      this.idbService.getConnection()
        .then(conn => new DailyregistersDaoService(conn))
        .then(dao => dao.findByDate(date))
        .then(dailyregister => {
          if (!dailyregister) {
            this.cashregisterService.get(1).subscribe((cashregister) => {
              let daily: Dailyregister = {
                id: null,
                date: date,
                cashregister,
                timeOpened: new Date(),
                timeClosed: null,
                entries: null
              }
              this.save(daily).subscribe(
                () => {
                  this.idbService.getConnection()
                    .then(conn => new DailyregistersDaoService(conn))
                    .then(dao => dao.findByDate(date))
                    .then(dailyregister => {
                      subscriber.next(dailyregister);
                      subscriber.complete();
                    });
                }
              );
            })

          } else {
            subscriber.next(dailyregister);
            subscriber.complete();
          }
        })
        .catch(erro => {
          console.log(erro);
          subscriber.error("Erro ao buscar registro diário data: " + date);
          subscriber.complete();
        });
    });
  }

  save(dailyregister: Dailyregister) {
    //console.log("ou esse id: " + dailyregister.id);
    if (!dailyregister.id || dailyregister.id == 0) {
      delete dailyregister.id;
      return new Observable(subscriber => {
        this.idbService.getConnection()
          .then(conn => new DailyregistersDaoService(conn))
          .then(dao => dao.insert(dailyregister))
          .then(() => {
            subscriber.next('Registro diário adicionado com sucesso');
            subscriber.complete();
          })
          .catch(erro => {
            console.log(erro);
            subscriber.error("Erro ao inserir registro diário");
            subscriber.complete();
          });
      });
    } else {
      return new Observable(subscriber => {
        this.idbService.getConnection()
          .then(conn => new DailyregistersDaoService(conn))
          .then(dao => dao.update(dailyregister))
          .then(() => {
            subscriber.next('Registro diário editado com sucesso');
            subscriber.complete();
          })
          .catch(erro => {
            console.log(erro);
            subscriber.error("Erro ao editar registro diário");
            subscriber.complete();
          });
      });
    }
  }



}
