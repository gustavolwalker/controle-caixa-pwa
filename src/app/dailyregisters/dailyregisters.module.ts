import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VMessageModule } from '../shared/components/vmessage/vmessage.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptor } from '../core/auth/request.interceptor';
import { DailyregistersDashComponent } from './dailyregisters-dash/dailyregisters-dash.component';
import { DailyregistersEntryComponent } from './dailyregisters-entry/dailyregisters-entry.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CalculatorModule } from '../shared/components/calculator/calculator.module';
import { SortableModule } from '../shared/directives/sortable/sortable.module';
import { SliceListModule } from '../shared/pipes/slice-list/slice-list.module';

@NgModule({
  declarations: [
    DailyregistersDashComponent,
    DailyregistersEntryComponent    
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    VMessageModule,
    CalculatorModule,
    SortableModule,
    SliceListModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    }
  ]
})
export class DailyregistersModule { }
