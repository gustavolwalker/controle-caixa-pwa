import { Component, OnInit, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DailyregistersService } from '../dailyregisters.service';
import { Dailyregister } from '../dailyregister';
import { DailyregisterEntry } from '../dailyregister-entry';
import { Salesman } from 'src/app/salesmans/salesman';
import { Paymentmethod } from 'src/app/paymentmethods/paymentmethod';
import { Paymentcondition } from 'src/app/paymentconditions/paymentcondition';
import { SortEvent, SortableDirective } from 'src/app/shared/directives/sortable/sortable.directive';

@Component({
    selector: 'app-dailyregisters-entry',
    templateUrl: './dailyregisters-entry.component.html'
})
export class DailyregistersEntryComponent implements OnInit {

    showActions: boolean = false;
    dailyregister: Dailyregister;
    entries: DailyregisterEntry[];
    entryForm: FormGroup;
    salesmans: Salesman[];
    defaultPaymentMethod: Paymentmethod;
    methods: Paymentmethod[];
    defaultPaymentCondition: Paymentcondition;
    conditions: Paymentcondition[];
    displayValue: String = new String();
    @ViewChild('form', { static: true }) ngForm;
    private formInputs = {
        value: [
            null,
            [
                Validators.required,
                Validators.min(-1000.01)
            ]
        ],
        salesman: [
            null,
            [
                Validators.required
            ]
        ],
        paymentMethod: [
            null,
            [
                Validators.required
            ]
        ],
        paymentCondition: [
            null,
            [
                Validators.required
            ]
        ],
        credit: [true],
        description: ['venda diária'],
        type: ['V'],
    };
    @ViewChildren(SortableDirective) headers: QueryList<SortableDirective>;

    constructor(
        private formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private service: DailyregistersService,
        private location: Location,
    ) { }

    ngOnInit(): void {

        this.entryForm = this.formBuilder.group(this.formInputs);

        this.activatedRoute.params.subscribe(() => {
            if (this.activatedRoute.snapshot.data['dailyregister']) {
                this.dailyregister = this.activatedRoute.snapshot.data['dailyregister'];
                if (this.dailyregister) {
                    this.entries = this.service.listAllEntries(this.dailyregister);
                    if (this.activatedRoute.snapshot.data['salesmans'])
                        this.salesmans = this.activatedRoute.snapshot.data['salesmans'];

                    if (this.activatedRoute.snapshot.data['paymentmethods']) {
                        this.methods = this.activatedRoute.snapshot.data['paymentmethods'];
                        this.methods.forEach((method) => {
                            if (method.nickName === this.dailyregister.cashregister.defaultPaymentMethod.nickName) {
                                this.defaultPaymentMethod = method;
                                this.entryForm.patchValue({
                                    paymentMethod: this.defaultPaymentMethod
                                });
                            }
                        });
                    }

                    if (this.activatedRoute.snapshot.data['paymentconditions']) {
                        this.conditions = this.activatedRoute.snapshot.data['paymentconditions'];
                        this.conditions.forEach((condition) => {
                            if (condition.nickName === this.dailyregister.cashregister.defaultPaymentCondition.nickName) {
                                this.defaultPaymentCondition = condition;
                                this.entryForm.patchValue({
                                    paymentCondition: this.defaultPaymentCondition
                                });

                            }
                        });
                    }
                } else
                    this.location.back();
            } else
                this.location.back();
        });
        this.activatedRoute.queryParams.subscribe((params) => {
            if (params['actions'])
                this.showActions = params['actions'] as boolean;
        });
    }

    private resetForm() {

        this.ngForm.resetForm();
        this.entryForm = this.formBuilder.group(this.formInputs);
        this.entryForm.patchValue({
            paymentMethod: this.defaultPaymentMethod,
            paymentCondition: this.defaultPaymentCondition
        });
        this.displayValue = new String();
        this.entries = this.service.listAllEntries(this.dailyregister);
    }

    save() {
        if (this.entryForm.valid && !this.entryForm.pending) {
            const entry = this.entryForm.getRawValue() as DailyregisterEntry;
            //console.log("id: " + entry.id);
           // console.log("entries: "+this.dailyregister.entries);
            if (!this.dailyregister.entries || this.dailyregister.entries.length <= 0) {
                this.dailyregister.entries = new Array<DailyregisterEntry>();
                entry.id = 1;
            } else {
                entry.id = this.dailyregister.entries[this.dailyregister.entries.length - 1].id + 1;
            }
          //  console.log("esse id: " + entry.id);
            entry.credit = entry.value >= 0;
            entry.value = entry.value < 0 ? entry.value as number * -1 : entry.value;
            this.dailyregister.entries.push(entry);

            this.service
                .save(this.dailyregister)
                .subscribe(() => {
                    this.resetForm();
                });
        }
    }


    remove(id: number) {

        this.dailyregister.entries =
            this.dailyregister.entries.filter(
                entry => entry.id != id
            )

        this.service
            .save(this.dailyregister)
            .subscribe(() => {
                this.resetForm();
            });
    }

    onSort({ column, direction }: SortEvent) {

        this.headers.forEach(header => {
            if (header.sortable !== column) {
                header.direction = '';
            }
        });
        this.service.sortColumn = column;
        this.service.sortDirection = direction;
        this.entries = this.service.listAllEntries(this.dailyregister);

    }



}
