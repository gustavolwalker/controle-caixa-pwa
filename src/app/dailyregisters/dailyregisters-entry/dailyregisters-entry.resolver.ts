import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { DailyregistersService } from '../dailyregisters.service';
import { Dailyregister } from '../dailyregister';

@Injectable({ providedIn: 'root' })
export class DailyregistersEntryResolver implements Resolve<Observable<Dailyregister>>{
             
    constructor(private service: DailyregistersService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Dailyregister> {
        let id: number = +route.params.id;
        if (id)
            return this.service.get(id);
    }
}