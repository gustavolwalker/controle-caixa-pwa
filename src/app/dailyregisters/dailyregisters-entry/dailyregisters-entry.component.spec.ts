import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyregistersEntryComponent } from './dailyregisters-entry.component';

describe('DailyregistersEntryComponent', () => {
  let component: DailyregistersEntryComponent;
  let fixture: ComponentFixture<DailyregistersEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyregistersEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyregistersEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
