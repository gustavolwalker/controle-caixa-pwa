import { TestBed } from '@angular/core/testing';

import { DailyregistersService } from './dailyregisters.service';

describe('DailyregistersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DailyregistersService = TestBed.get(DailyregistersService);
    expect(service).toBeTruthy();
  });
});
