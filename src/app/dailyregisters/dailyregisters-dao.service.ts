import { Inject } from '@angular/core';
import { Dailyregister } from './dailyregister';

@Inject({
  providedIn: 'root'
})
export class DailyregistersDaoService {

  private store = 'dailyregisters';
  constructor(private connection: IDBDatabase) { }

  insert(dailyregister: Dailyregister) {

    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .add(dailyregister);

      request.onsuccess = () => {

        resolve();
      };

      request.onerror = () => {

        console.log(request.error);
        reject('Não foi possível inserir o registro diário');

      };
    });
  }

  update(dailyregister: Dailyregister) {

    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .put(dailyregister);

      request.onsuccess = () => {

        resolve();
      };

      request.onerror = () => {

        console.log(request.error);
        reject('Não foi possível atualizar o registro diário');

      };
    });
  }

  delete(id: number){
    return new Promise((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readwrite')
        .objectStore(this.store)
        .delete(id);

      request.onsuccess = () => {

        resolve();
      };

      request.onerror = () => {

        console.log(request.error);
        reject('Não foi possível excluir o registro diário');

      };
    });
  }

  findById(id: number) {

    return new Promise<Dailyregister>((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readonly')
        .objectStore(this.store)
        .get(id);

      request.onsuccess = () => {
        
        resolve(request.result);
      };

      request.onerror = () => {

        console.log(request.error);
        reject(`Não foi possível encontrar o registro diário: ${id}`);
      };
    });
  }

  findByDate(date: Date) {

    return new Promise<Dailyregister>((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readonly')
        .objectStore(this.store)
        .index('idxDate')
        .get(date);

      request.onsuccess = e => {

        console.log(`Dao get date ${date} result ${request.result}`);
        resolve(request.result);
      };

      request.onerror = e => {

        console.log(request.error);
        reject(`Não foi possível encontrar o registros diário data: ${date}`);
      };
    });
  }

  findAll(page: number) {
    return new Promise<Dailyregister[]>((resolve, reject) => {

      let request = this.connection
        .transaction([this.store], 'readonly')
        .objectStore(this.store)
        .getAll();

      request.onsuccess = e => {

        resolve(request.result);
      };

      request.onerror = e => {

        console.log(request.error);
        reject(`Não foi possível buscar os registros diários`);
      };
    });
  }


}

