import { Paymentcondition } from '../paymentconditions/paymentcondition';
import { Salesman } from '../salesmans/salesman';
import { Paymentmethod } from '../paymentmethods/paymentmethod';

export interface DailyregisterEntry{
    id: number;
    credit: boolean;
    description: string;
    value: Number;
    salesman: Salesman;
    paymentCondition: Paymentcondition;
    paymentMethod: Paymentmethod;
    type: string;
}