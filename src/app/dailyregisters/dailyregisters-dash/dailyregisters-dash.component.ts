import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DailyregistersService } from '../dailyregisters.service';

@Component({
  selector: 'app-dailyregisters-dash',
  templateUrl: './dailyregisters-dash.component.html',
  styleUrls: ['./dailyregisters-dash.component.css']
})
export class DailyregistersDashComponent implements OnInit {

  dailyregistersForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: DailyregistersService,

  ) { }

  ngOnInit() {

    this.dailyregistersForm = this.formBuilder.group({
      date: [
        new Date().toISOString().substring(0, 10),
        [
          Validators.required
        ]
      ]
    });
  }

  open() {

    if (this.dailyregistersForm.valid && !this.dailyregistersForm.pending) {
      const data = this.dailyregistersForm.get('date').value;
      if (data) {
        this.service.openDate(new Date(data + "T00:00:00")).subscribe(
          dailyregister => {
            console.log(`Component navigate to: ${dailyregister}`);
            if (dailyregister)
              this.router.navigate(['diario', dailyregister.id])
          }
        );
      }
    }
  }

  close() {

    if (this.dailyregistersForm.valid && !this.dailyregistersForm.pending) {
      const data = this.dailyregistersForm.get('data');
      console.log('deve fechar o diario: ' + data.value);
    }
  }
}
