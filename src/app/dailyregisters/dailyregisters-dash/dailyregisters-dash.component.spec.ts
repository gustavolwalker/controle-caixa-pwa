import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyregistersDashComponent } from './dailyregisters-dash.component';

describe('DailyregistersDashComponent', () => {
  let component: DailyregistersDashComponent;
  let fixture: ComponentFixture<DailyregistersDashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyregistersDashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyregistersDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
