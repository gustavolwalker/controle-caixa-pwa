import { TestBed } from '@angular/core/testing';

import { DailyregistersDaoService } from './dailyregisters-dao.service';

describe('DailyregistersDaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DailyregistersDaoService = TestBed.get(DailyregistersDaoService);
    expect(service).toBeTruthy();
  });
});
