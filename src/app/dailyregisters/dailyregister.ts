import { DailyregisterEntry } from './dailyregister-entry';
import { Cashregister } from '../cashregister/cashregister';

export interface Dailyregister{
    id: Number;
    date: Date;
    cashregister: Cashregister;
    timeOpened: Date;
    timeClosed: Date;
    entries: DailyregisterEntry[];
}