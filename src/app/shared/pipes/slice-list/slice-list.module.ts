import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliceListPipe } from './slice-list.pipe';

@NgModule({
  declarations: [SliceListPipe],
  exports: [SliceListPipe],
  imports: [CommonModule]
})
export class SliceListModule { }
