import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sliceList'
})
export class SliceListPipe implements PipeTransform {

  transform(values: [], size: number): [[]] {
    let parentArray: [[]] = [[]];
    let childArray: [] = [];
    values.forEach((item, index) => {
      childArray.push(item);
      if (childArray.length === size || index === values.length - 1) {
        parentArray.push(childArray);
        childArray = [];
      }
    });
    return parentArray;
  }

}
