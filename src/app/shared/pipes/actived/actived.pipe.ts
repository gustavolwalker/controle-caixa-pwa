import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'actived'
})
export class ActivedPipe implements PipeTransform {

  transform(value: boolean): any {
    
    return value ? 'Ativo' : 'Inativo';
  }

}
