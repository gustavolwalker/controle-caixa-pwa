import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivedPipe } from './actived.pipe';

@NgModule({
  declarations: [ActivedPipe],
  exports: [ActivedPipe],
  imports: [CommonModule]
})
export class ActivedModule { }
