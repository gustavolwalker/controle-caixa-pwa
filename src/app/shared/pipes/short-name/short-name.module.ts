import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShortNamePipe } from './short-name.pipe';

@NgModule({
  declarations: [ShortNamePipe],
  exports: [ShortNamePipe],
  imports: [CommonModule]

})
export class ShortNameModule { }
