import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortName'
})
export class ShortNamePipe implements PipeTransform {


  transform(value: string): string {

    let acronimos = ['sr', 'sra', 'srta', 'mr', 'ms', 'mrs', 'miss', 'dr'];

    if (!value) return '';
    return (
      value.split(' ')[0] + ' ' +
      (
        acronimos.indexOf(value.split(' ')[0].toLowerCase()) > -1 ?
          value.split(' ')[1] :
          ''
      )
    ).trim();
  }

}
