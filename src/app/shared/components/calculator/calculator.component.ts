import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']

})
export class CalculatorComponent {

  @ViewChild('valueInput', { static: true }) valueInput: ElementRef<HTMLInputElement>;
  @ViewChild('displayTextArea', { static: false }) displayTextArea: ElementRef<HTMLTextAreaElement>;
  @Input() parentForm: FormGroup;
  @Input() showDisplay: boolean;
  @Input() display: String;

  pressButtom(value: any) {
    if (Number.isInteger(value) || value === '.')
      this.display = this.display.concat(`${value}`);
    else {
      if (value !== '=') {
        if (this.display.substring(this.display.lastIndexOf('\n') + 1).trim() === "") {
          this.display = this.display.concat(
            this.display
              .substring(
                this.display.lastIndexOf('=') + 1,
                this.display.lastIndexOf('\n')
              )
              .trim()
          );
        }
        this.display = this.display.concat(` ${value} `);
      } else {
        try {
          let result = this.calcResult(this.display.substring(this.display.lastIndexOf('\n') + 1));
          this.display = this.display.concat(` ${value} ${result}\n`);
          this.parentForm.patchValue({ value: result });
        } catch (e) {
          console.log(e);
          this.display = this.display.concat(` ${value} {error}\n`);
        }
      }
    }
    this.scrollDisplayToBottom();
  }

  calcResult(calc: string): number {

    return eval(calc);
  }

  scrollDisplayToBottom(): void {

    try {
      this.displayTextArea.nativeElement.scrollTop = this.displayTextArea.nativeElement.scrollHeight;
    } catch (err) { }
  }
}
