import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterListComponent } from './filter-list.component';

@NgModule({
  declarations: [FilterListComponent],
  exports: [FilterListComponent],
  imports: [CommonModule]
})
export class FilterListModule { }
